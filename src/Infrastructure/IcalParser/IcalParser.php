<?php

namespace App\Infrastructure\IcalParser;

final class IcalParser
{
    public function execute(string $ical): array
    {
        $lines = explode("\n", $ical);
        $events = [];

        foreach ($lines as $key => $line) {
            switch (true) {
                case str_starts_with($line, 'BEGIN:VEVENT'):
                    $event = [];
                    break;
                case str_starts_with($line, 'DTSTART'):
                    $event['start_at'] = $this->extractValueLine($line);
                    break;
                case str_starts_with($line, 'DTEND'):
                    $event['end_at'] = $this->extractValueLine($line);
                    break;
                case str_starts_with($line, 'UID'):
                    $event['airbnb_uid'] = $this->extractValueLine($line);
                    break;
                case str_starts_with($line, 'DESCRIPTION'):
                    $event['airbnb_id'] = $this->extractAirbnbIdLine($lines[$key + 1]);
                    break;
                case str_starts_with($line, 'SUMMARY'):
                    $event['status'] = $this->extractValueLine($line);
                    break;
                case str_starts_with($line, 'END:VEVENT'):
                    $events[] = $event ?? [];
                    break;
            }
        }

        return $events;
    }

    private function extractValueLine(string $line): string
    {
        return substr($line, strpos($line, ':') + 1);
    }

    private function extractAirbnbIdLine(string $line): string
    {
        return strstr(substr($line, 9), "\\", true);
    }
}