<?php

namespace App\Infrastructure\IcalParser;

final class IcalEvent
{
    private \DateTimeInterface $startAt;

    private \DateTimeInterface $endAt;

    private string $airbnbUid;

    private ?string $airbnbId = null;

    private string $status;

    public function getStartAt(): \DateTimeInterface
    {
        return $this->startAt;
    }

    public function setStartAt(\DateTimeInterface $startAt): self
    {
        $this->startAt = $startAt;

        return $this;
    }

    public function getEnAt(): \DateTimeInterface
    {
        return $this->endAt;
    }

    public function setEndAt(\DateTimeInterface $endAt): self
    {
        $this->endAt = $endAt;

        return $this;
    }

    public function getAirbnbUid(): string
    {
        return $this->airbnbUid;
    }

    public function setAirbnbUid(string $airbnbUid): self
    {
        $this->airbnbUid = $airbnbUid;

        return $this;
    }

    public function getAirbnbId(): ?string
    {
        return $this->airbnbId;
    }

    public function setAirbnbId(?string $airbnbId): self
    {
        $this->airbnbId = $airbnbId;

        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }
}