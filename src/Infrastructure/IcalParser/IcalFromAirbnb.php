<?php

namespace App\Infrastructure\IcalParser;

use Symfony\Component\HttpClient\Exception\ClientException;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

final class IcalFromAirbnb
{
    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function execute(string $icalUrl): string
    {
        try {
            $client = HttpClient::create();
            $response = $client->request('GET', $icalUrl);

            if ($response->getStatusCode() === 200) {
                return $response->getContent();
            }
        } catch (ClientException $e) {

        }

        return '';
    }
}