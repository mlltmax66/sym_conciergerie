<?php

namespace App\Infrastructure\IcalParser;

use App\Domain\Reservation\Entity\ReservationStatusEnum;

final class IcalEventFactory
{
    public function create(array $event): ?IcalEvent
    {
        if (empty($event['start_at']) ||
            empty($event['end_at']) ||
            empty($event['airbnb_uid']) ||
            empty($event['status'])
        ) {
            return null;
        }

        return (new IcalEvent())
            ->setStartAt($this->strToDatetime($event['start_at']))
            ->setEndAt($this->strToDatetime($event['end_at']))
            ->setAirbnbUid($event['airbnb_uid'])
            ->setAirbnbId($event['airbnb_id'] ?? null)
            ->setStatus(ReservationStatusEnum::getIcalEventStatusEnum($event['status']));
    }

    private function strToDatetime(string $string): \DateTimeInterface
    {
        return \DateTime::createFromFormat('Ymd', $string);
    }
}