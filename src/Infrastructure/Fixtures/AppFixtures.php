<?php

namespace App\Infrastructure\Fixtures;

use App\Domain\Housing\Entity\Housing;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

final class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $housing = (new Housing())
            ->setName('Housing')
            ->setIcalUrl('https://www.airbnb.fr/calendar/ical/53503938.ics?s=66d60b449d86b89a9309536d1be23a6f')
            ->setCountReservation(0);

        $manager->persist($housing);
        $manager->flush();
    }
}
