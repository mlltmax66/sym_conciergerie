<?php

namespace App\Command;

use App\Domain\Housing\Repository\HousingRepository;
use App\Domain\Reservation\Services\Ical\ReservationIcalService;
use App\Infrastructure\IcalParser\IcalEvent;
use App\Infrastructure\IcalParser\IcalEventFactory;
use App\Infrastructure\IcalParser\IcalFromAirbnb;
use App\Infrastructure\IcalParser\IcalParser;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

#[AsCommand('app:reservation-ical')]
final class ReservationIcalCommand extends Command
{
    public function __construct(
        private readonly HousingRepository      $housingRepository,
        private readonly IcalFromAirbnb         $icalFromAirbnb,
        private readonly IcalParser             $icalParser,
        private readonly IcalEventFactory       $icalEventFactory,
        private readonly ReservationIcalService $reservationService,
        ?string                                 $name = null
    )
    {
        parent::__construct($name);
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $housings = $this->housingRepository->findAll();

        foreach ($housings as $housing) {
            $ical = $this->icalFromAirbnb->execute($housing->getIcalUrl());

            $eventsParsed = $this->icalParser->execute($ical);

            $events = [];
            foreach ($eventsParsed as $eventParsed) {
                $event = $this->icalEventFactory->create($eventParsed);

                if ($event instanceof IcalEvent) {
                    $events[] = $event;
                }
            }

            $this->reservationService->execute($housing, $events);
        }

        return Command::SUCCESS;
    }
}
