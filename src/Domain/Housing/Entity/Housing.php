<?php

namespace App\Domain\Housing\Entity;

use App\Domain\Housing\Repository\HousingRepository;
use App\Domain\Reservation\Entity\Reservation;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: HousingRepository::class)]
final class Housing
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private string $name;

    #[ORM\Column(length: 255)]
    private string $ical_url;

    #[ORM\Column]
    private int $count_reservation;

    /**
     * @var Collection<int, Reservation>
     */
    #[ORM\OneToMany(targetEntity: Reservation::class, mappedBy: 'housing')]
    private Collection $reservations;

    public function __construct()
    {
        $this->reservations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIcalUrl(): string
    {
        return $this->ical_url;
    }

    public function setIcalUrl(string $icalUrl): self
    {
        $this->ical_url = $icalUrl;

        return $this;
    }

    public function getCountReservation(): int
    {
        return $this->count_reservation;
    }

    public function setCountReservation(int $count_reservation): self
    {
        $this->count_reservation = $count_reservation;

        return $this;
    }

    /**
     * @return Collection<int, Reservation>
     */
    public function getReservations(): Collection
    {
        return $this->reservations;
    }

    public function addReservation(Reservation $reservation): self
    {
        if (!$this->reservations->contains($reservation)) {
            $this->reservations->add($reservation);
            $reservation->setHousing($this);
        }

        return $this;
    }

    public function removeReservation(Reservation $reservation): self
    {
        if ($this->reservations->removeElement($reservation)) {
            if ($reservation->getHousing() === $this) {
                $reservation->setHousing(null);
            }
        }

        return $this;
    }
}
