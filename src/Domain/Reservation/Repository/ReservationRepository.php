<?php

namespace App\Domain\Reservation\Repository;

use App\Domain\Reservation\Entity\Reservation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Reservation>
 *
 * @method Reservation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Reservation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Reservation[]    findAll()
 * @method Reservation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
final class ReservationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Reservation::class);
    }

    /**
     * @param array<string> $eventsUid
     * @return Reservation[]
     */
    public function findAllNotInIcal(array $eventsUid): array
    {
        return $this->createQueryBuilder('r')
            ->where('r.airbnb_uid NOT IN (:eventsUid)')
            ->setParameter('eventsUid', $eventsUid)
            ->getQuery()
            ->getResult();
    }
}
