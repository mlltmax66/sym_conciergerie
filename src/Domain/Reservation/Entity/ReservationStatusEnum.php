<?php

namespace App\Domain\Reservation\Entity;

enum ReservationStatusEnum
{
    const RESERVED = 'reserved';

    const BLOCKED = 'blocked';

    public static function getIcalEventStatusEnum(string $status): ?string
    {
        return match ($status) {
            'Reserved' => ReservationStatusEnum::RESERVED,
            'Airbnb (Not available)' => ReservationStatusEnum::BLOCKED,
            default => null,
        };
    }
}
