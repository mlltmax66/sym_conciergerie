<?php

namespace App\Domain\Reservation\Entity;

use App\Domain\Housing\Entity\Housing;
use App\Domain\Reservation\Repository\ReservationRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ReservationRepository::class)]
final class Reservation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $airbnb_id = null;

    #[ORM\Column(length: 255)]
    private string $airbnb_uid;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private \DateTimeInterface $start_at;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private \DateTimeInterface $end_at;

    #[ORM\Column(length: 255)]
    private string $status;

    #[ORM\ManyToOne(inversedBy: 'reservations')]
    private Housing $housing;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAirbnbId(): ?string
    {
        return $this->airbnb_id;
    }

    public function setAirbnbId(?string $airbnb_id): self
    {
        $this->airbnb_id = $airbnb_id;

        return $this;
    }

    public function getAirbnbUid(): string
    {
        return $this->airbnb_uid;
    }

    public function setAirbnbUid(string $airbnb_uid): self
    {
        $this->airbnb_uid = $airbnb_uid;

        return $this;
    }

    public function getStartAt(): \DateTimeInterface
    {
        return $this->start_at;
    }

    public function setStartAt(\DateTimeInterface $start_at): self
    {
        $this->start_at = $start_at;

        return $this;
    }

    public function getEndAt(): \DateTimeInterface
    {
        return $this->end_at;
    }

    public function setEndAt(\DateTimeInterface $end_at): self
    {
        $this->end_at = $end_at;

        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getHousing(): Housing
    {
        return $this->housing;
    }

    public function setHousing(Housing $housing): self
    {
        $this->housing = $housing;

        return $this;
    }
}
