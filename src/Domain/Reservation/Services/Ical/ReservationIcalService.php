<?php

namespace App\Domain\Reservation\Services\Ical;

use App\Domain\Housing\Entity\Housing;
use App\Domain\Reservation\Entity\Reservation;
use App\Domain\Reservation\Repository\ReservationRepository;
use App\Infrastructure\IcalParser\IcalEvent;
use Doctrine\ORM\EntityManagerInterface;

final readonly class ReservationIcalService
{
    public function __construct(
        private EntityManagerInterface    $em,
        private ReservationRepository     $reservationRepository,
        private CreateReservation         $createReservation,
        private UpdateReservation         $updateReservation,
        private DeleteOrCancelReservation $deleteOrCancelReservation,
    )
    {

    }

    /**
     * @param Housing $housing
     * @param IcalEvent[] $events
     * @return void
     */
    public function execute(Housing $housing, array $events): void
    {
        $countReservation = $housing->getCountReservation();

        foreach ($events as $event) {
            $reservationExist = $this->reservationExist($event);

            if ($reservationExist) {
                $this->updateReservation->update($event, $reservationExist);
            } else {
                $this->createReservation->create($event, $housing);
                $countReservation++;
            }
        }

        $countEvents = count($events);
        if ($countReservation > $countEvents) {
            $this->deleteOrCancelReservation->deleteOrCancel($events);
        }

        $housing->setCountReservation($countEvents);
        $this->em->flush();
    }

    private function reservationExist(IcalEvent $event): ?Reservation
    {
        return $this->reservationRepository->findOneBy(['airbnb_uid' => $event->getAirbnbUid()]);
    }
}
