<?php

namespace App\Domain\Reservation\Services\Ical;

use App\Domain\Housing\Entity\Housing;
use App\Domain\Reservation\Entity\Reservation;
use App\Infrastructure\IcalParser\IcalEvent;
use Doctrine\ORM\EntityManagerInterface;

final readonly class CreateReservation
{
    public function __construct(private EntityManagerInterface $em)
    {

    }

    public function create(IcalEvent $event, Housing $housing): void
    {
        $reservation = (new Reservation())
            ->setStartAt($event->getStartAt())
            ->setEndAt($event->getEnAt())
            ->setAirbnbId($event->getAirbnbId())
            ->setAirbnbUid($event->getAirbnbUid())
            ->setStatus($event->getStatus())
            ->setHousing($housing);

        $this->em->persist($reservation);
        $this->em->flush();
    }
}
