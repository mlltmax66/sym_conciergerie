<?php

namespace App\Domain\Reservation\Services\Ical;

use App\Domain\Reservation\Entity\Reservation;
use App\Infrastructure\IcalParser\IcalEvent;
use Doctrine\ORM\EntityManagerInterface;

final readonly class UpdateReservation
{
    public function __construct(private EntityManagerInterface $em)
    {

    }

    public function update(IcalEvent $event, Reservation $reservation): void
    {
        if ($reservation->getAirbnbId() !== $event->getAirbnbId() ||
            $reservation->getStartAt()->format('Y-m-d') !== $event->getStartAt()->format('Y-m-d') ||
            $reservation->getEndAt()->format('Y-m-d') !== $event->getEnAt()->format('Y-m-d')
        ) {
            $reservation
                ->setAirbnbId($event->getAirbnbId())
                ->setStartAt($event->getStartAt())
                ->setEndAt($event->getEnAt());

            $this->em->flush();
        }
    }
}
