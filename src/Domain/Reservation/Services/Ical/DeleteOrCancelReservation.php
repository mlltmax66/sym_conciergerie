<?php

namespace App\Domain\Reservation\Services\Ical;

use App\Domain\Reservation\Entity\Reservation;
use App\Domain\Reservation\Repository\ReservationRepository;
use App\Infrastructure\IcalParser\IcalEvent;
use Doctrine\ORM\EntityManagerInterface;

final readonly class DeleteOrCancelReservation
{
    public function __construct(
        private ReservationRepository  $reservationRepository,
        private EntityManagerInterface $em,
    )
    {

    }

    /**
     * @param IcalEvent[] $events
     * @return void
     */
    public function deleteOrCancel(array $events): void
    {
        $eventsUid = array_map(function (IcalEvent $event) {
            return $event->getAirbnbUid();
        }, $events);

        $reservations = $this->reservationRepository->findAllNotInIcal($eventsUid);

        $today = new \DateTime();

        foreach ($reservations as $reservation) {
            if ($this->reservationNotStarted($reservation, $today)) {
                $this->em->remove($reservation);
            } elseif ($this->reservationPast($reservation, $today)) {
                $this->em->remove($reservation);
            } elseif ($this->reservationInProgress($reservation, $today)) {
                $reservation->setEndAt($today);
            }
        }

        $this->em->flush();
    }

    private function reservationNotStarted(Reservation $reservation, \DateTime $today): bool
    {
        return $reservation->getStartAt()->format('Y-m-d') > $today->format('Y-m-d');
    }

    private function reservationPast(Reservation $reservation, \DateTime $today): bool
    {
        return $reservation->getEndAt()->format('Y-m-d') < $today->format('Y-m-d');
    }

    private function reservationInProgress(Reservation $reservation, \DateTime $today): bool
    {
        return $reservation->getStartAt()->format('Y-m-d') <= $today->format('Y-m-d') &&
            $reservation->getEndAt()->format('Y-m-d') >= $today->format('Y-m-d');
    }
}
